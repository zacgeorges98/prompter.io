module.exports = {
  apps: [
    {
      name: "3007-prompter.io",
      script: "npm run build && npm run start",
      // args: "",
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: "1G",
      cwd: "/opt/homebrew/var/www/zag98/prompter.io/",
      env: {
        NODE_ENV: "production",
        PORT: "3007",
        NEXT_PUBLIC_SANITY_PROJECT_ID: "xxidweiz",
        NEXT_PUBLIC_SANITY_DATASET: "production",
      }
    },
    {
      name: "3006-prompter.io-dev",
      script: "npm run dev",
      instances: 1,
      autorestart: true,
      watch: true,
      max_memory_restart: "1G",
      cwd: "/opt/homebrew/var/www/zag98/prompter.io/",
      env: {
        NODE_ENV: "development",
        PORT: "3006",
      },
    },
  ],
};
