"use client";

import { SessionProvider } from "next-auth/react";

const Provider = ({ children, session }) => (
  <SessionProvider
    session={session}
    options={{ basePath: `/prompter.io/api/auth` }}
  >
    {children}
  </SessionProvider>
);

export default Provider;
